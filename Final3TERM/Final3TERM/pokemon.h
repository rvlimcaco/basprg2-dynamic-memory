#pragma once
#include <string>
#include <iostream>

using namespace std;

class pokemon {

public:
	pokemon();
	pokemon(int baseHp, int basedamage, int level, int exp, int expToNextLevel);

	int baseHp;
	int hp;
	int level;
	int basedamage; //random here
	int exp;
	int expToNextLevel;
	int pokemonSize = 1;

	int catchRate = rand() % 10 + 1;

	string wildpokemon[15]{ "Chespin" , "Fennekin", "Froakie", "Bunnelby", "Fletchling" , "ScatterBug" ,
		"Spewpa", "Vivillon", "Litleo", "Flabebe", "Skiddo", "Pancham", "Furfrou", "Espurr", "Honeedge" };
	string allowedPokemon[6];

	void starter(int starterPick);
	void capture(int encounter, int catchRate, int allowedPokemon);
};