#pragma once
#include <string>
#include <iostream>
#include "pokemon.h"

using namespace std;

class trainer {

public:
	trainer();
	string name;
	int teamSize;
	int starterPokemon;
	string starter[3]{ "Chespin", "Fennikin", "Froakie" };

	trainer(string name);

	void playermovement();
	void welcomeScreen(string name);
	void starterpick();


};